import sys

class Calc():

    def add(self, op1 , op2 ):
        return op1 + op2

    def sub(self, op1 , op2):
        return op1 - op2

if __name__ == "__main__":
    try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")

    if sys.argv[2] == "+":
        operacion = Calc()
        print(operacion.add(operand1,operand2))
    elif sys.argv[2] == "-":
        operacion = Calc()
        print(operacion.sub(operand1,operand2))
    else:
        sys.exit('Operand should be + or -')