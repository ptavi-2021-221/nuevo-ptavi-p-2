import sys
from calcoo import Calc

class CalcChild(Calc):

    def mul(self ,op1,op2):
        return op1 * op2

    def div(self, op1, op2):
        try:
            return op1 / op2
        except ZeroDivisionError:
            print("Division by zero is not allowed")
            raise ValueError

if __name__ == "__main__":

    try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")

    if sys.argv[2] == "+":
        operacion = CalcChild()
        print(operacion.add(operand1, operand2))
    elif sys.argv[2] == "-":
        operacion = CalcChild()
        print(operacion.sub(operand1, operand2))
    elif sys.argv[2] == "x":
        operacion = CalcChild()
        print(operacion.mul(operand1, operand2))
    elif sys.argv[2] == "/":
        operacion = CalcChild()
        print(operacion.div(operand1, operand2))
    else:
        sys.exit('Operand should be + , x , / or -')